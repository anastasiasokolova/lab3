#include <stdio.h>
#include <stdlib.h>
#include <locale.h>


int sum_arr(const int arr[], size_t size)
{
    int sum = 0;
    for(int i = 0; i< size; i++)
        sum += arr[i];
    return sum;
}

int scalar(const int a[], const int b[], size_t vectorSize)
{
	int sum = 0;
	int i;

	for(i = 0; i < vectorSize; i++)
		sum += a[i] * b[i];
	return sum;
}

int is_prime(unsigned long n)
{
	unsigned long i;

	if(n <= 1) return 0;
	for(i = 2; i*i <= n; i++)
		if(n % i == 0) return 0;
	return 1;
}

int main(int argc, char** argv)
{
	size_t vectorSize;
	size_t vectorSize2;
	unsigned long num;
	int *x;
	int *first_arr;
	int *second_arr;

	printf("1. Enter vector size: ");
	scanf("%lu", &vectorSize2);

	x =  (int*)malloc(vectorSize2 * sizeof(int));
	
	printf ("\nEnter vector: ");
	for (int i = 0; i < vectorSize2; ++i) {
		scanf("%d", (x+i));
	}

	printf( "Sum of numbers is: %d\n\n", sum_arr(x, vectorSize2));
	
	printf("Enter vector size: ");
	scanf("%lu", &vectorSize);
	
	first_arr =  (int*)malloc(vectorSize * sizeof(int));
	second_arr =  (int*)malloc(vectorSize * sizeof(int));
	
	printf ("\nEnter first vector: ");
	for (int i = 0; i < vectorSize; ++i) {
		scanf("%d", (first_arr+i));
	}
	printf ("\nEnter second vector: ");
	for (int i = 0; i < vectorSize; ++i) {
		scanf("%d", (second_arr+i));
	}

	printf( "Inner product: %d\n\n", scalar( first_arr, second_arr,  vectorSize));

	printf("3. Enter number to check the primary: ");
	scanf("%lu", &num);

	printf("Number %s\n", is_prime(num) ? "is prime" : "is not prime");
	return 0;
}
